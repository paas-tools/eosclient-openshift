#! /bin/sh
#
# The image supports the keytab to be passed either as mounted volume in $KEYTAB_PATH
# or the username and password of an account in KEYTAB_USER and KEYTAB_PWD

# Allow a custom SELinuxlevel to be passed as an argument. This will change the
# SELinux level on the credentials set by eosfusebind to add an extra layer of security
# and avoid applications using eos to change credentials of other apps.
SELINUX_CUSTOM_LEVEL=$1

# Abort if there are more than one keytab
if [[ $(ls -1 $KEYTAB_PATH | wc -l) != 1 ]];
then
  # If the keytab is in an environment variable, dump it into a file in $KEYTAB_PATH
  if [[ ! -z $KEYTAB_USER  && ! -z $KEYTAB_PWD ]]; then
    echo "Generating a keytab for '$KEYTAB_USER' using password in KEYTAB_PWD environment variable..."
    echo -e "addent -password -p $KEYTAB_USER@CERN.CH -k 1 -e rc4-hmac\n$KEYTAB_PWD\nwkt $KEYTAB_PATH/secret.keytab\nquit" | ktutil
  else
    echo "ERROR: A single keytab must be mounted in '${KEYTAB_PATH}' or a user and password combination for the user must be passed in 'KEYTAB_USER' and 'KEYTAB_PWD'"
    exit 1
  fi
fi

USER_ID=$(id -u)
KTAB_CACHE=$KEYTAB_PATH/*
HOST_KRB5_CACHE=/tmp/user.krb5
EOS_CREDENTIALS_FILE="${EOS_CREDENTIALS_PATH}/store/uid${USER_ID}.krb5"

# Obtain kerberos ticket to run eosfusebind, this ticket will not be renewed
k5start -f $KTAB_CACHE -k $HOST_KRB5_CACHE -U &> /dev/null
if [[ ! $? -eq 0 ]]; then
  echo "Failure to obtain a ticket for user ${KEYTAB_USER} with provided password. Please verify if they are valid and if the account is activated."
  exit 2
fi

# Bind current user id with the krb ticket
eosfusebind -g krb5 $HOST_KRB5_CACHE

# eosfusebind will copy the ticket to $EOS_CREDENTIALS_FILE
if [[ ! -f ${EOS_CREDENTIALS_FILE} ]]; then
  echo "Failure to run eosfusebind!"
  exit 1
fi

# Change level of eosfusebind credentials if a custom SELinux level is provided
if [[ ! -z $SELINUX_CUSTOM_LEVEL ]]; then
  echo "Changing SELinux level of eosfusebind credentials to '${SELINUX_CUSTOM_LEVEL}' ..."
  chcon -l $SELINUX_CUSTOM_LEVEL "${EOS_CREDENTIALS_PATH}/uid${USER_ID}.lock"

  # Store the chcon command in the AKLOG so it is run every time k5start renews the ticket cache. (ref. k5start man page)
  # This is needed as k5start will change the SELinux level every rime the cache is renewed
  export AKLOG="chcon -l $SELINUX_CUSTOM_LEVEL $EOS_CREDENTIALS_FILE && echo '$EOS_CREDENTIALS_FILE successfully renewed'"
else
  export AKLOG="echo '$EOS_CREDENTIALS_FILE successfully renewed'"
fi

echo 'eosfusebind successfully completed - Running automatic renewal of kerberos tickets'
exec k5start -f $KTAB_CACHE -k $EOS_CREDENTIALS_FILE -K 30 -l 24h -U -t
